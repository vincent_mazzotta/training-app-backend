//import dependencies modules
const express = require('express');
//init express (create the server)
var app = express();
//port number
const port =3000;
app.listen(port,function(){
    console.log('Server started at port:'+port);
});

//Test server
app.get('/', (req, res)=>{res.send(`Hello World from  ${process.env.FIRST_NAME || "<No First Name!>"} ${process.env.LAST_NAME || "<No Last Name!>"}`);});


